import java.io.*;
import java.util.*;

public class Pl {
    static String url; //= "C:\\Users\\beyza\\Desktop\\1.txt"; 
    static String[] set1;
    static String[] set2;

    public static void main(String[] args) throws FileNotFoundException {
		
		
		url = args[0];
		
        Scanner scanner = new Scanner(new FileReader(url));
        int size = 0;
        while (scanner.hasNext()){
            size = size + 1;
            scanner.next();
        }
        scanner.close();
        set1 = new String[size];
        set2 = new String[size];
        scanner = new Scanner(new FileReader(url));
		String line;
        for(int i = 0; scanner.hasNext(); i++){
			line = scanner.next();
            set1[i] = line;
            set2[i] = line;
        }


        System.out.println("Bubble sorting time: " + bubbleSort(set1) + " ns");
        System.out.println("Selection sorting time: " + selectionSort(set2) + " ns");
		/*for(int i = 0; i<set1.length; i++){
			System.out.println(set1[i] + " " + set2[i]);
		}*/

    }

    private static Long bubbleSort(String[] set1) {
        Long startTime = System.nanoTime();
        String temp;
        for (int j = 0; j < set1.length; j++) {
            for (int i = j + 1; i < set1.length; i++) {
                if (set1[i].compareTo(set1[j]) > 0) {
                    temp = set1[j];
                    set1[j] = set1[i];
                    set1[i] = temp;
                }
            }
        }
        Long endTime = System.nanoTime();
        return (endTime - startTime) ;
    }

    public static Long selectionSort( String[] set ) {
        Long startTime = System.nanoTime();
        for ( int j=0; j < set.length-1; j++ ) {

            int min = j;
            for ( int k=j+1; k < set.length; k++ )
                if ( set[k].compareTo( set[min] ) > 0 ) min = k;
            
            String temp = set[j];
            set[j] = set[min];
            set[min] = temp;
        }
        Long endTime = System.nanoTime();
        return (endTime - startTime) ;
    }
}
