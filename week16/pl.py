import timeit
import sys
url = sys.argv[1]

list1 = []
list2 = []
f = open(url, "r")
line = f.readline()
line = line.replace("\n", "")
while line:
	list1.append(line)
	list2.append(line)	
	line = f.readline()
	line = line.replace("\n", "")
f.close()


def bubbleSort(alist):
	for passnum in range(len(alist)-1,0,-1):
			for i in range(passnum):
				if alist[i]>alist[i+1]:
					temp = alist[i]
					alist[i] = alist[i+1]
					alist[i+1] = temp


def selectionSort(alist):
	for fillslot in range(len(alist)-1,0,-1):
		   positionOfMin=0
		   for location in range(1,fillslot+1):
			   if alist[location]>alist[positionOfMin]:
				   positionOfMin = location

		   temp = alist[fillslot]
		   alist[fillslot] = alist[positionOfMin]
		   alist[positionOfMin] = temp


startb = timeit.default_timer() * 1000000000
bubbleSort(list1)
endb = timeit.default_timer() * 1000000000
print "Bubble sorting time: ", (endb - startb), " ns"

start = timeit.default_timer() * 1000000000
selectionSort(list2)
end = timeit.default_timer() * 1000000000
print "Selection sorting time: ", (end - start), " ns"